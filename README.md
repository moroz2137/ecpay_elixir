# ECPay 綠界 Payment Gateway Integration for Elixir

The purpose of this library is to provide API integration with the ECPay (綠界科技)
payment gateway provided by the Taiwan-based Green World FinTech Service Co., Ltd.
(綠界科技股份有限公司).

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `ecpay` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:ecpay, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/ecpay](https://hexdocs.pm/ecpay).

