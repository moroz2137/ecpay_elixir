defmodule ECPay.Invoice do
  alias ECPay.{Config, AIOParams, Checksum}

  defstruct transaction_no: nil,
            timestamp: nil,
            total: 0,
            item_name: nil,
            item_count: nil,
            item_price: nil,
            item_word: nil,
            item_amount: nil,
            tax_type: "1",
            donation: false,
            print: false,
            customer_email: nil,
            customer_phone: nil

  def request(%__MODULE__{} = params) do
    HTTPoison.post(Config.invoice_url(), to_post_data(params), [
      {"Content-Type", "application/x-www-form-urlencoded"}
    ])
  end

  def to_post_data(%__MODULE__{} = params) do
    map = to_map(params)
    mac_value = Checksum.calculate(map, :invoice)

    data =
      for {key, val} <- map do
        "#{key}=#{URI.encode(to_string(val))}&"
      end

    [data, "CheckMacValue=#{mac_value}"]
  end

  defp generate_token(length) do
    :crypto.strong_rand_bytes(length) |> Base.encode16()
  end

  def to_map(params) do
    %{
      "TimeStamp" => normalize_timestamp(params.timestamp),
      "MerchantID" => Config.merchant_id(),
      "RelateNumber" => params.transaction_no || generate_token(10),
      "Print" => "0",
      "Donation" => "2",
      "TaxType" => params.tax_type,
      "SalesAmount" => params.total,
      "CustomerEmail" => params.customer_email,
      "InvType" => "07",
      "ItemName" => normalize_list(params.item_name),
      "ItemCount" => normalize_list(params.item_count),
      "ItemWord" => normalize_list(params.item_word),
      "ItemPrice" => normalize_list(params.item_price),
      "ItemAmount" => normalize_list(params.item_amount)
    }
  end

  def normalize_list(string) when is_binary(string), do: string
  def normalize_list(int) when is_integer(int), do: Integer.to_string(int)
  def normalize_list(list) when is_list(list), do: Enum.join(list, "|")

  def normalize_bool(t) when t in [0, "0", false, nil, "false"], do: "0"
  def normalize_bool(f) when f in [1, "1", true, "true"], do: "1"

  def normalize_timestamp(nil), do: :os.system_time(:seconds)
  def normalize_timestamp(unix) when is_integer(unix), do: unix

  def normalize_timestamp(%NaiveDateTime{} = time) do
    time |> Timex.to_datetime("Etc/UTC") |> Timex.to_unix()
  end

  def normalize_timestamp(%DateTime{} = time) do
    time |> Timex.to_unix()
  end
end
