defmodule ECPay.Checksum do
  alias ECPay.AIOParams, as: Params
  alias ECPay.Invoice
  alias ECPay.Config

  def calculate(params, service \\ :aio)

  @spec calculate(params :: ECPay.AIOParams.t() | map()) :: String.t()
  def calculate(%module{} = params, service) when module in [Params, Invoice] do
    module.to_map(params)
    |> calculate(service)
  end

  def calculate(params, service) when is_map(params) do
    params
    |> encode_map_as_query(service)
    |> uri_escape()
    |> hash(service)
  end

  def verify(%{"CheckMacValue" => checksum} = params) do
    case calculate(params) == checksum do
      true ->
        :ok

      false ->
        {:error, :checksum}
    end
  end

  def uri_escape(string) do
    string
    |> URI.encode_www_form()
    |> String.downcase()
    |> String.replace("%40", "%2540")
    |> String.replace("%257c", "%7c")
  end

  def hash_values(map, :aio) do
    hash_key = Map.get(map, "HashKey", Config.hash_key())
    hash_iv = Map.get(map, "HashIV", Config.hash_iv())
    {hash_key, hash_iv}
  end

  def hash_values(map, :invoice) do
    hash_key = Map.get(map, "HashKey", Config.invoice_hash_key())
    hash_iv = Map.get(map, "HashIV", Config.invoice_hash_iv())
    {hash_key, hash_iv}
  end

  def hash_function(:aio), do: :sha256
  def hash_function(:invoice), do: :md5

  def encode_pair({key, val}, :aio) do
    "#{key}=#{val}"
  end

  def encode_pair({key, val}, :invoice) do
    "#{key}=#{URI.encode(to_string(val))}"
  end

  def forbidden_values(:aio) do
    ~w[CheckMacValue HashKey HashIV]
  end

  def forbidden_values(:invoice) do
    ~w[CheckMacValue HashKey HashIV ItemName ItemWord InvoiceRemark ItemRemark]
  end

  @spec encode_map_as_query(map()) :: binary()
  def encode_map_as_query(map, service \\ :aio) when is_map(map) do
    {hash_key, hash_iv} = hash_values(map, service)

    forbidden = forbidden_values(service)

    query_str =
      Enum.filter(map, fn {k, v} -> !is_nil(v) && to_string(k) not in forbidden end)
      |> Enum.map(fn pair -> encode_pair(pair, service) end)
      |> Enum.join("&")

    "HashKey=#{hash_key}&" <> query_str <> "&HashIV=#{hash_iv}"
  end

  defp hash(value, service) do
    :crypto.hash(hash_function(service), value) |> Base.encode16(case: :upper)
  end
end
